# Address Book

## Usage
Make the script executable:<br>
`chmod +x address_book.sh`
<br><br>
Run the script<br>
`./address_book.sh`
<br><br>
Interact with the address book menu <br>
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   1. Add a user <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  2. Edit a user  <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  3. Search a user <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  4. Show all users <br>
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5. Delete a user <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  6.Exit