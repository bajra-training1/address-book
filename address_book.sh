ADDRESS_BOOK='myfile.txt'

add_a_user()
{
	read -p 'Enter name: ' name
	read -p 'Enter address: ' address
	read -p 'Enter phone number: ' phone

	echo "$name    $address    $phone" >> "$ADDRESS_BOOK"
	echo 'USER INFORMATION ADDED SUCCESSFULLY!'
}

edit_user_info()
{
    read -p 'Enter the name of the user to edit: ' target_name
    read -p 'Enter new address: ' new_address
    read -p 'Enter new phone number: ' new_phone

    grep -q "^$target_name" "$ADDRESS_BOOK"
    if [ $? -eq 0 ]; then
        grep -v "^$target_name" "$ADDRESS_BOOK" > temp_file
        echo "$target_name    $new_address    $new_phone"
        mv temp_file "$ADDRESS_BOOK" 
        echo "$target_name EDITED SUCCESSFULLY!"
    else
        echo "USER NOT FOUND!"
    fi
}

search_user()
{
    read -p 'Enter the name of the user to search: ' target_name
    grep -i "$target_name" "$ADDRESS_BOOK"
    if [ $? -ne 0 ]; then
        echo 'USER NOT FOUND!'
    fi
}

show_user()
{
    cat "$ADDRESS_BOOK"
}

delete_a_user()
{
    read -p 'Enter phone number of the user to delete: ' target_phone
    grep -q "$target_phone" "$ADDRESS_BOOK"
    if [ $? -eq 0 ]; then
        grep -v "$target_phone" "$ADDRESS_BOOK" > temp_file
        mv temp_file "$ADDRESS_BOOK"
        echo "$target_phone DELETED SUCCESSFULLY!"
    else
        echo "USER NOT FOUND!"
    fi
}

while true; do
    echo '--------------------------ADDRESS BOOK MENU--------------------------------------'

    echo 'Enter your option:'
    echo '1. Add a user'
    echo '2. Edit a user'
    echo '3. Search a user'
    echo '4. Show all users'
    echo '5. Delete a user'
    echo '6. Exit'
    echo '---------------------------------------------------------------------------------'

    read option

    case $option in
        1) add_a_user ;;
        2) edit_user_info ;;
        3) search_user ;;
        4) show_user ;;
        5) delete_a_user ;;
        6) echo 'Exiting the script'; exit ;;
        *) echo "INVALID OPTION!";;
    esac
done